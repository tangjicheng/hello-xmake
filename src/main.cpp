#include <iostream>

#include <spdlog/spdlog.h>

#include "utils.h"
#include "u2.h"

int main(int argc, char** argv) {
    std::cout << "hello world!" << std::endl;
    
    spdlog::info("SPDLOG: {}", __FILE__);

    return 0;
}
