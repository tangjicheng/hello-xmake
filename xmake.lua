-- 定义一个函数来设置通用编译选项
function set_common_flags()
    set_symbols("debug")
    add_cxxflags("-g", "-Wall", "-Wextra")
    if not is_plat("macosx") then
        add_cxxflags("-static-libstdc++", "-static-libgcc")
    end
    -- add_includedirs("/usr/local/include")
    add_includedirs("3rd")
    add_linkdirs("/usr/local/lib")
end

-- 定义目标
target("hello")
    set_kind("binary")
    add_files("src/*.cpp")
    -- add_links("mylib")
    -- 调用函数设置通用编译选项
    set_common_flags()
